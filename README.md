
<h1> Angular 15 &amp; Java 11 working together</h1>
<h2> Prepared for SDA </h2>

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.4.


## The Server side runs Spring Boot application on port:8080 
To build the springboot server you use [gradle](https://docs.gradle.org/current/userguide/userguide.html)

* cd sda-angular-demo 
* gradle clean build
* gradle run --info 

The Angular is build and run on Node.JS 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


<!-- include: HELP.md -->

{{include(HELP.md)}}


<h3> Questions used to ask ChatGPT for the project: </h3>


<pre> TypeScript requires that the enum member names start with a capital letter when using PascalCase naming convention. Here's the corrected code: 

enum Day {
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday
};

or 

enum Day {
  Monday = 'Monday',
  Tuesday = 'Tuesday',
  Wednesday = 'Wednesday',
  Thursday = 'Thursday',
  Friday = 'Friday'
};


</pre>

Defining interfaces in TS:

<pre>

interface Person {
  firstName: string;
  lastName: string;
}

class Greeter {
  greeting: Person;

  constructor(message: Person) {
    this.greeting = message;
  }

  greet(): string {
    return "Hello, " + this.greeting.firstName + " " + this.greeting.lastName;
  }
}

let user: Person = { firstName: 'Jan', lastName: 'Kowalski' };

let greeter = new Greeter(user);

console.log(greeter.greet());

</pre>
<br/>

in Typescript show the examples of Annotations: 

<pre>

function log(target: any, key: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;

  descriptor.value = function (...args: any[]) {
    console.log(`Calling ${key} with arguments: ${JSON.stringify(args)}`);
    const result = originalMethod.apply(this, args);
    console.log(`Method ${key} returned: ${result}`);
    return result;
  };

  return descriptor;
}

class Example {
  @log
  greet(name: string): string {
    return `Hello, ${name}!`;
  }
}

const example = new Example();
example.greet("John");


In this example, the @log decorator is applied to the greet method of the Example class. The decorator function receives three 
parameters: the target (class prototype), the method name (key), and the property descriptor. The decorator modifies the behavior 
of the method by wrapping the original method with additional logging statements.


</pre>


Define what decorator is in OOP sense using TS code 

<pre>

In object-oriented programming (OOP), a decorator is a design pattern that allows behavior to be added to an individual 
object dynamically, without affecting the behavior of other objects of the same class. In TypeScript, decorators are a 
language feature that enables you to apply decorators to classes, methods, properties, or parameters, providing a way 
to modify their behavior or add metadata.


</pre>


Show the simple code in Java for Decorator for better understanding

<pre>

// Component interface
interface Beverage {
    String getDescription();
    double getCost();
}

// Concrete component
class Coffee implements Beverage {
    @Override
    public String getDescription() {
        return "Coffee";
    }

    @Override
    public double getCost() {
        return 2.0;
    }
}

// Decorator
abstract class BeverageDecorator implements Beverage {
    protected Beverage beverage;

    public BeverageDecorator(Beverage beverage) {
        this.beverage = beverage;
    }
}

// Concrete decorator
class MilkDecorator extends BeverageDecorator {
    public MilkDecorator(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Milk";
    }

    @Override
    public double getCost() {
        return beverage.getCost() + 0.5;
    }
}

public class DecoratorExample {
    public static void main(String[] args) {
        // Creating a coffee with milk
        Beverage coffeeWithMilk = new MilkDecorator(new Coffee());
        System.out.println("Description: " + coffeeWithMilk.getDescription());
        System.out.println("Cost: $" + coffeeWithMilk.getCost());
    }
}


In this example, we have a Beverage interface representing the component. The Coffee class is a concrete implementation of Beverage.

The BeverageDecorator abstract class is the decorator, which implements the Beverage interface and holds a reference to 
the wrapped component. The MilkDecorator is a concrete decorator that adds milk to the beverage.

In the DecoratorExample class, we create a Coffee object and then wrap it with the MilkDecorator to add milk. When we 
call getDescription() and getCost() on the decorated beverage, it returns the description and cost of the coffee with milk.

</pre>


And as "Java Annotation" too? 

<pre>

import java.lang.annotation.*;

// Custom annotation
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Log {
}

// Class with annotated method
class Example {
    @Log
    public void performOperation() {
        System.out.println("Performing operation...");
    }
}

public class AnnotationExample {
    public static void main(String[] args) throws NoSuchMethodException {
        Example example = new Example();
        example.performOperation(); // Output will be logged due to the annotation
    }
}

</pre>


Explain using @Slf4J annotation

<pre>

The @Slf4j annotation is a popular annotation used in Java for automatic generation of logging boilerplate code. It is part of the Lombok library, which provides various annotations to simplify Java code development.

To use @Slf4j, follow these steps:

    Make sure you have the Lombok library added as a dependency in your project.

    Add the @Slf4j annotation to your class.

    Use the generated logger object to perform logging operations.

Here's an example that demonstrates the usage of @Slf4j annotation:


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoggingExample {

    public static void main(String[] args) {
        log.debug("Debug message");
        log.info("Info message");
        log.warn("Warning message");
        log.error("Error message");
    }
}


</pre>

Can you explain what Angular selector is? 

<pre>

In Angular, the selector is a way to identify and use components, directives, or elements in your templates. It is used 
to match and instantiate a specific component or directive when it appears in an Angular template.

The selector is typically defined as an attribute value in the component's metadata, such as @Component or @Directive 
decorators. It can take different forms based on the selector types supported by Angular:

    Element Selector: An element selector matches a specific HTML element. It is represented by the element name in the 
component's metadata. For example:

@Component({
  selector: 'app-example',
  ...
})

* Class Selector: A class selector matches an HTML element that has a specific CSS class applied. It is represented by 
the class name preceded by a dot (.) in the component's metadata. For example:

@Component({
  selector: '.highlight',
  ...
})

* Attribute Selector: An attribute selector matches an HTML element that has a specific attribute with a specific value. 
It is represented by the attribute name surrounded by square brackets ([]) in the component's metadata. For example:

@Component({
  selector: '[appCustomDirective]',
  ...
})


In Angular, the selector is a way to identify and use components, directives, or elements in your templates. It is used to match and instantiate a specific component or directive when it appears in an Angular template.

The selector is typically defined as an attribute value in the component's metadata, such as @Component or @Directive decorators. It can take different forms based on the selector types supported by Angular:

    Element Selector: An element selector matches a specific HTML element. It is represented by the element name in the component's metadata. For example:

typescript

@Component({
  selector: 'app-example',
  ...
})

In this case, the component with the selector app-example can be used in a template by simply using <app-example></app-example>.

    Class Selector: A class selector matches an HTML element that has a specific CSS class applied. It is represented by the class name preceded by a dot (.) in the component's metadata. For example:

typescript

@Component({
  selector: '.highlight',
  ...
})

In this case, the component with the selector .highlight can be used in a template by applying the CSS class to an element, like <div class="highlight"></div>.

Attribute Selector: An attribute selector matches an HTML element that has a specific attribute with a specific value. It is represented by the attribute name surrounded by square brackets ([]) in the component's metadata. For example:

typescript

@Component({
  selector: '[appCustomDirective]',
  ...
})

In this case, the directive with the selector [appCustomDirective] can be used in a template by applying the attribute 
to an element, like <div appCustomDirective></div>.

These are the main selector types in Angular. The selector helps Angular identify the component, directive, or element 
to instantiate or apply when rendering templates. It provides a way to encapsulate and reuse functionality across 
different parts of an application.

</pre>

Tell more about ElementRef in Angular

<pre>

In Angular, ElementRef is a class provided by the Angular framework that represents a reference to a DOM element. It 
allows you to access and manipulate the underlying native element in your component or directive.

The ElementRef class is part of the @angular/core package, and it is typically injected into the constructor of a 
component or directive using Angular's dependency injection mechanism.

Here's an example of how you can use ElementRef in an Angular component:

---
import { Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-example',
  template: `<div #myElement>Hello, ElementRef!</div>`
})
export class ExampleComponent {
  constructor(private elementRef: ElementRef) {
    // Access the native element
    const nativeElement = elementRef.nativeElement;
    console.log(nativeElement.textContent); // Output: Hello, ElementRef!
  }
}


</pre>

Explain Directive in Angular

<pre>

In Angular, a directive is a class that allows you to add custom behavior to elements or 
components in your application. Directives enable you to extend the functionality of HTML 
elements, create reusable components, and manipulate the DOM.

Here's an example of an attribute directive that changes the background color of an element:

---
import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  @HostListener('mouseenter')
  onMouseEnter() {
    this.highlight('yellow');
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.highlight(null);
  }

  private highlight(color: string | null) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', color);
  }
}

---
<div appHighlight>
  Hover over me to see the background color change!
</div>


</pre>

Show the same in JavaScript 

<pre>

In JavaScript, you can achieve the equivalent functionality of the @HostListener('mouseenter') decorator in Angular by 
manually attaching an event listener to the element using JavaScript's addEventListener method. Here's an example:

---
constructor() {
  const element = document.querySelector('[appHighlight]');
  element.addEventListener('mouseenter', this.onMouseEnter.bind(this));
}

onMouseEnter() {
  this.highlight('yellow');
}

highlight(color) {
  // Perform the desired logic to highlight the element
  const element = document.querySelector('[appHighlight]');
  element.style.backgroundColor = color;
}

</pre>

Define and explain a Pipe in Angular


<pre>

In Angular, a pipe is a feature that allows you to transform or format data within a template. Pipes are a type of 
Angular's built-in or custom feature that provides a convenient way to perform common data transformations.

Pipes are represented as a keyword followed by a pipe operator (|) in the template syntax. They take an input value and 
apply a transformation to produce an output value that is then displayed in the template.

Angular provides several built-in pipes that you can use out of the box, such as DatePipe, UpperCasePipe, LowerCasePipe, 
and more. These pipes cover a wide range of formatting and transformation tasks.

To use a pipe in Angular, you typically follow this syntax within a template:

---
{{ value_expression | pipe_name[:optional_argument] }}

Here's an example using the DatePipe to format a date value:

---
&lt;p&gt;Today's date is {{ currentDate | date }}&lt;p&gt;


Here's an example of a custom pipe that appends "USD" to a currency value:

---
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencySuffix'
})
export class CurrencySuffixPipe implements PipeTransform {
  transform(value: number): string {
    return value + ' USD';
  }
}

---
<code>
&lt;p&gt;Total amount: {{ amount | currencySuffix }}&lt;p&gt;
</code>

</pre>


I want a json pipe example for Student class that takes name, surname and DOB year

<pre>

---
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
name: 'json'
})
export class JsonPipe implements PipeTransform {
transform(value: any): string {
return JSON.stringify(value);
}
}

In this example, we create a custom pipe called JsonPipe that implements the PipeTransform interface. 
The transform method takes a value of any type and uses JSON.stringify() to convert it into a JSON string representation.

To use the JsonPipe in your template for the Student class, you can apply it to an instance of the Student object as follows:

---
&lt;p&gt;{{ student | json }}&lt;/p&gt;

or multiple pipes:

&lt;p&gt;{{ student | json | uppercase }}&lt;/p&gt;
</pre>

What providers are in Angular?

<pre>

In Angular, providers are a fundamental concept used for dependency injection. They define how instances of services, 
values, or other dependencies are created and made available throughout your application.


In Angular, providers are a fundamental concept used for dependency injection. They define how instances of services, values,<br> or other dependencies are created and made available throughout your application.

Here are the main types of providers in Angular:

    Class Providers: Class providers are used to provide instances of classes as dependencies. You typically register a service <br>class as a provider so that Angular can create instances of the service when needed. Class providers are defined using the provide function in the @Injectable decorator <br>or using the providers array in a module.

    Value Providers: Value providers are used to provide simple values as dependencies. Instead of creating an instance of a class, <br>you can provide a specific value, such as a string, number, or boolean. Value providers are defined using the provide function in the @Injectable decorator or using the <br>providers array in a module.

    Factory Providers: Factory providers are used to provide instances of dependencies using custom factory functions. You can define a <br>factory function that creates and returns an instance of a dependency. Factory providers are defined using the provide function in the @Injectable decorator or using <br>the providers array in a module.

    Existing Providers: Existing providers are used when you want to reuse an existing dependency registered in Angular's injector. <br>You can provide an existing instance of a dependency by specifying the dependency token in the useExisting property of the provider definition.

    Aliased Providers: Aliased providers are used to provide a different token to refer to an existing dependency. You can create an alias <br>for a dependency by specifying the dependency token in the useExisting property of the provider definition.

    Multi Providers: Multi providers are used when you want to provide multiple values for a single dependency token. You can register multiple <br>providers for the same token using the multi: true property in the provider definition.

Providers are typically defined at the module level using the providers array in the @NgModule decorator. This allows the providers to be available <br>throughout the module and its components. However, providers can also be defined at the component level using the providers property in the @Component <br>decorator. This makes the providers available<br>only to that specific component and its child components.

Overall, providers are essential in Angular for managing dependencies and enabling dependency injection, which promotes modular, reusable, and testable code.

</pre>

Define Controller and Service 


<pre>

In the context of web development, "Controller" and "Service" are common terms used in different frameworks and architectures. Let's define each term:

    Controller:
    A controller is a component or class that acts as an intermediary between the user interface and the business logic of an application. It is responsible for <br>handling user actions, manipulating data, and updating the view. In the Model-View-Controller (MVC) architectural pattern, the controller receives input from the user, <br>interacts with the model (data), and updates the view (user interface). Controllers help separate concerns <br>by isolating user interactions and business logic from the view.

    Service:
    A service is a reusable component or class that encapsulates a specific functionality or business logic in an application. Services are typically used to handle <br>data operations, communicate with external APIs or databases, perform calculations, or provide utility functions. They are designed to be independent and can be injected into <br>multiple components, promoting code reusability and maintainability. Services are often used to manage shared <br>data and provide a centralized place for application logic <br>that is not specific to a particular component.

</pre>


How to use GET for accessing web API? 

<pre>

---
export class WeatherData {
  location: string;
  temperature: number;
  description: string;
  humidity: number;
  windSpeed: number;
  // ... other properties as per your needs

  constructor(data: Partial<WeatherData>) {
    Object.assign(this, data);
  }
}



---
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WeatherData } from './weather-data.model'; // Assuming you have a WeatherData model defined

@Injectable()
export class WeatherService {
  private apiUrl = 'https://api.weatherapi.com/v1/forecast.json';
  private apiKey = 'YOUR_API_KEY'; // Replace with your actual API key

  constructor(private http: HttpClient) {}

  getWeatherData(location: string): Observable<WeatherData> {
    const url = `${this.apiUrl}?key=${this.apiKey}&q=${location}&days=5`; // Example URL with query parameters

    return this.http.get<WeatherData>(url);
  }
}


</pre>