package com.decorators.demo;

import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class DecoratorAsAnnotation {

    public static void main(String[] args) {

        new DecoratorAsAnnotation().thisOneToTest();
    }

    public DecoratorAsAnnotation thisOneToTest() {

        System.out.println(
        org.apache.commons.lang3.StringUtils.isAlphanumeric(
         new String(",").replaceAll(" ", "").replaceAll(",","" ) ) );

        final List<String> nullSafeList = Arrays.asList( new String[] { "one, two", null, null } )
                .stream()
                .map(StringUtils::defaultString)  // Convert nulls to empty strings
                .collect(Collectors.toList());

        System.out.println( nullSafeList );

        log.debug("Debug message for list: {}", nullSafeList );
        log.info("Info message for list: {}" , nullSafeList );
        log.warn("Warning message for list {}", nullSafeList );
        log.error("Error message for list {}", nullSafeList );

        return this;
    }
}





