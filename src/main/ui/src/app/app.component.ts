import { Component, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppService } from './app.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  @ViewChild('emailAlertDiv', { static: false }) emailAlertDiv: ElementRef;

  constructor(private appService: AppService) {}

  title = 'angular-nodejs-example';

  // Inside your component class
  userForm = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email])
  });

  users: any[] = [];
  userCount = 0;

  destroy$: Subject<boolean> = new Subject<boolean>();

  onSubmit() {
    this.appService.addUser(this.userForm.value, this.userCount + 1).pipe(takeUntil(this.destroy$)).subscribe(data => {
      console.log('message::::', data);
      this.userCount = this.userCount + 1;
      console.log(this.userCount);
      this.userForm.reset();
    });
  }

  getAllUsers() {
    this.appService.getUsers().pipe(takeUntil(this.destroy$)).subscribe((users: any[]) => {
		this.userCount = users.length;
        this.users = users;
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  ngOnInit() {
	this.getAllUsers();
  };

  validateEmail(email) : boolean {

        // Weak check -faster
        // const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        const emailRegex: RegExp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

        // console.log( "Check for regexp: ", email);
        return emailRegex.test(email);
  };

  handleEmailChange() : void {

   const email = this.userForm.get('email').value;
   const classNames: string[] = ['red', 'green'];

   // Clear all style at the beginning
   const divElement: HTMLElement = this.emailAlertDiv.nativeElement;

      classNames.forEach(className => {
         divElement.classList.remove( `{className}-alert` );
      });

    if (email === '') {
      // Handle empty input

        divElement.textContent = 'Please enter your email.';
        divElement.classList.add('amber-alert');

    } else if (! this.validateEmail( email )) {

     // Handle invalid email
        divElement.textContent = 'Email entered is invalid.'; // Change the text content
        divElement.classList.add('red-alert');

    } else {

      // Handle valid email
        divElement.textContent = 'Email can be submitted now.'; // Change the text content
        divElement.classList.add('green-alert');

    }
  }

  isEmailInvalid() : boolean {

    const email = (this.userForm.get('email') as FormControl).value;

    console.log('Validating email:', email);
    return this.validateEmail( email );
  }


}
