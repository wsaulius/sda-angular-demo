package com.bbtutorials.users;

import com.decorators.demo.DecoratorAsAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

@Slf4j
public class JavaAnnotationTest {

    @Mock
    DecoratorAsAnnotation decoratorAsAnnotation;

    @BeforeEach
    public void setUp() {

        MockitoAnnotations.openMocks(this);
        // Initialize any required resources before each test method
        when(decoratorAsAnnotation.thisOneToTest());

    }

    @Test
    public void testAnnotations() {

        decoratorAsAnnotation = new DecoratorAsAnnotation();
        decoratorAsAnnotation.thisOneToTest();

    }

}
